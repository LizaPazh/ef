﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs
{
    public class UserWithParameters
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTasks { get; set; }
        public int NotCompletedOrCanceledTasks { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
