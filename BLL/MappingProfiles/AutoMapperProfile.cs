﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Models;
using AutoMapper;

namespace BLL.MappingProfiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateTwoWaysMap<Project, ProjectDTO>();
            CreateTwoWaysMap<User, UserDTO>();
            CreateTwoWaysMap<ProjectTask, TaskDTO>();
            CreateTwoWaysMap<Team, TeamDTO>();
        }
        private void CreateTwoWaysMap<T1, T2>()
        {
            CreateMap<T1, T2>();
            CreateMap<T2, T1>();
        }

    }
}
