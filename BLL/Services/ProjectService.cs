﻿using AutoMapper;
using BLL.DTOs;
using BLL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class ProjectService: IProjectService
    {
        private readonly IProjectRepository _rep;
        private readonly IMapper _mapper;
        public ProjectService(IProjectRepository rep, IMapper mapper)
        {
            _rep = rep;
            _mapper = mapper;
        }
        public ProjectDTO GetProjectById(int projectId)
        {
            return _mapper.Map<ProjectDTO>(_rep.GetById(projectId));
        }
        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            var projects =  _mapper.Map<IEnumerable<ProjectDTO>>(_rep.GetAll());
            return projects;
        }
        public void CreateProject(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);
            _rep.Create(project);
        }
        public void UpdateProject(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);
            _rep.Update(project);
        }
        public void DeleteProject(int projectId)
        {
            _rep.Delete(projectId);
        }
        public Dictionary<ProjectDTO, int> CountTasksForProjectByUser(int userId)
        {
            return _mapper.Map<Dictionary<ProjectDTO, int>>(_rep.GetAll(p=>p.ProjectTasks).Where(p => p.AuthorId == userId)
                          .ToDictionary(y => y, y => y.ProjectTasks.Count()));
        }
        public IEnumerable<ProjectWithParameters> GetProjectWithCharacteristics()
        {
            return _rep.GetAll(p => p.ProjectTasks, p=>p.Team.Users).Select(p => new ProjectWithParameters
            {
                Project = _mapper.Map<ProjectDTO>(p),
                TaskByDescription = _mapper.Map<TaskDTO>(p.ProjectTasks.OrderByDescending(t => t.Description.Length).FirstOrDefault()),
                TaskByName = _mapper.Map<TaskDTO>(p.ProjectTasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                UsersCount = (p.Description.Length > 20 || p.ProjectTasks.Count() < 3) ? p.Team.Users.Count() : 0
            });
        }
    }
}
