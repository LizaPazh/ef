﻿using AutoMapper;
using BLL.DTOs;
using BLL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class TaskService: ITaskService
    {
        private readonly IProjectTaskRepository _rep;
        private readonly IMapper _mapper;
        public TaskService(IProjectTaskRepository rep, IMapper mapper)
        {
            _rep = rep;
            _mapper = mapper;
        }
        public TaskDTO GetTaskById(int taskId)
        {
            return _mapper.Map<TaskDTO>(_rep.GetById(taskId));
        }
        public IEnumerable<TaskDTO> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_rep.GetAll());
        }
        public void CreateTask(TaskDTO taskDTO)
        {
            var task = _mapper.Map<ProjectTask>(taskDTO);
            _rep.Create(task);
        }
        public void UpdateTask(TaskDTO taskDTO)
        {
            var task = _mapper.Map<ProjectTask>(taskDTO);
            _rep.Update(task);
        }
        public void DeleteTask(int taskId)
        {
            _rep.Delete(taskId);
        }
        public IEnumerable<TaskDTO> TasksByUser(int userId)
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_rep.GetAll().Where(t => t.PerformerId == userId && t.Name.Length < 45));
        }
        public IEnumerable<Tuple<int, string>> FinishedTasksByUser(int userId)
        {
            return _rep.GetAll().Where(t => t.PerformerId == userId && t.FinishedAt.Year == DateTime.Now.Year)
                         .Select(c => new Tuple<int, string>(c.Id, c.Name));
        }
    }
}
