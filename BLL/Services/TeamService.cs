﻿using AutoMapper;
using BLL.DTOs;
using BLL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly ITeamRepository _rep;
        private readonly IMapper _mapper;
        public TeamService(ITeamRepository rep, IMapper mapper)
        {
            _rep = rep;
            _mapper = mapper;
        }
        public TeamDTO GetTeamById(int teamId)
        {
            return _mapper.Map<TeamDTO>(_rep.GetById(teamId));
        }
        public IEnumerable<TeamDTO> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(_rep.GetAll());
        }
        public void CreateTeam(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);
            _rep.Create(team);
        }
        public void UpdateTeam(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);
            _rep.Update(team);
        }
        public void DeleteTeam(int teamId)
        {
            _rep.Delete(teamId);
        }
        public IEnumerable<TeamWithUsers> GetTeamsByUserAge()
        {
            return _rep.GetAll(t=>t.Users).Where(t => t.Users.Any() && t.Users.All(u => DateTime.Now.Year - u.Birthday.Year >= 10)).AsEnumerable()
                        .Select(t => { t.Users = t.Users.OrderByDescending(u => u.RegisteredAt); return t; })
                        .GroupBy(t => t)
                        .Select(g => new TeamWithUsers{
                            TeamId = g.Key.Id,
                            TeamName = g.Key.Name,
                            Users = _mapper.Map<IEnumerable<UserDTO>>(g.Key.Users)
                        });

        }
    }
}
