﻿using DAL.Context;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public interface ITeamRepository : IRepository<Team>
    {
    }
    public class TeamRepository : RepositoryBase<Team>, ITeamRepository
    {
        public TeamRepository(CompanyDBContext context)
              : base(context)
        { }
      
    }
}
