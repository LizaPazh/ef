﻿using DAL.Context;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        //IQueryable<User> GetUsers();
        //User GetByIdWithProjectsAndTasks(int userId);
    }
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(CompanyDBContext context)
              : base(context)
        { }
        //public IQueryable<User> GetUsers()
        //{
        //    return context.Users.Include(t => t.Projects).Include(t => t.ProjectTasks);
        //}
        //public User GetByIdWithProjectsAndTasks(int userId)
        //{
        //    return context.Users.Include(t => t.Projects).Include(t => t.ProjectTasks).SingleOrDefault(u => u.Id == userId);
        //}
    }
}
