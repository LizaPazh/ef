﻿using DAL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DAL
{
    public class RepositoryBase<T> : IRepository<T> where T : class
    {
        protected readonly CompanyDBContext context;
        public RepositoryBase(CompanyDBContext context)
        {
            this.context = context;
        }
        public void Create(T entity)
        {
            context.Add(entity);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = context.Set<T>().Find(id);

            if (entity != null)
            {
                context.Remove(entity);
            }
            context.SaveChanges();
        }
        public IQueryable<T> GetAll(params Expression<Func<T, object>>[] properties)
        {
            var query = context.Set<T>() as IQueryable<T>;

            if (properties == null)
                return query.AsNoTracking();

            return properties.Aggregate(query, (current, property) => current.Include(property));
        }

        public T GetById(int id)
        {
            return context.Set<T>().Find(id);
        }

        public void Update(T entity)
        {
            context.Set<T>().Update(entity);
            context.SaveChanges();
        }
    }
}
