﻿using System.Collections.Generic;
using System.Linq;
using BLL.DTOs;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService service;
        public ProjectsController(IProjectService projectService)
        {
            service = projectService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetProjects()
        {
            return service.GetAllProjects().ToList();
        }

        [HttpGet("{projectId}")]
        public ActionResult<ProjectDTO> GetProjectById(int projectId)
        {
            var project = service.GetProjectById(projectId);

            if(project == null)
            {
                return NotFound();
            }

            return project;
        }
        [HttpPost]
        public ActionResult CreateProject(ProjectDTO project)
        {
            service.CreateProject(project);
            return Ok();
        }

        [HttpPut]
        public ActionResult UpdateProject(ProjectDTO project)
        {
            var existingProject = service.GetProjectById(project.Id);

            if (existingProject == null)
            {
                return NotFound();
            }

            service.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete("{projectId}")]
        public ActionResult DeleteProject(int projectId)
        {
            var projectToDelete = service.GetProjectById(projectId);

            if(projectToDelete == null)
            {
                return NotFound();
            }

            service.DeleteProject(projectId);
            return NoContent();
        }

        [HttpGet("tasks/{userId}")]
        public ActionResult<List<KeyValuePair<ProjectDTO, int>>> CountTasksForProjectByUser(int userId)
        {           
            return service.CountTasksForProjectByUser(userId).ToList();
        }

        [HttpGet("projectsWithCharacteristics")]
        public ActionResult<IEnumerable<ProjectWithParameters>> GetProjectWithCharacteristics()
        {
            return service.GetProjectWithCharacteristics().ToList();
        }
    }
}