﻿using System.Collections.Generic;
using System.Linq;
using BLL.DTOs;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {

        private readonly ITeamService service;
        public TeamsController(ITeamService teamService)
        {
            service = teamService;
        }
        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetTeams()
        {
            return service.GetAllTeams().ToList();
        }

        [HttpGet("{teamId}")]
        public ActionResult<TeamDTO> GetTeamById(int teamId)
        {
            var team = service.GetTeamById(teamId);

            if(team == null)
            {
                return NotFound();
            }

            return team;
        }
        [HttpPost]
        public ActionResult CreateTeam(TeamDTO team)
        {
            service.CreateTeam(team);
            return Ok();
        }
        [HttpPut]
        public ActionResult UpdateTeam(TeamDTO team)
        {
            var existingTeam = service.GetTeamById(team.Id);

            if (existingTeam == null)
            {
                return NotFound();
            }

            service.UpdateTeam(team);
            return NoContent();
        }

        [HttpDelete("{teamId}")]
        public ActionResult DeleteTeam(int teamId)
        {
            var teamToDelete = service.GetTeamById(teamId);

            if (teamToDelete == null)
            {
                return NotFound();
            }

            service.DeleteTeam(teamId);
            return NoContent();
        }

        [HttpGet("teamsByUserAge")]
        public ActionResult<IEnumerable<TeamWithUsers>> GetTeamsByUserAge()
        {

            return service.GetTeamsByUserAge().ToList();
        }
    }
}