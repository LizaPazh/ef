﻿using System.Collections.Generic;
using System.Linq;
using BLL.DTOs;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService service;
        public UsersController(IUserService teamService)
        {
            service = teamService;
        }
        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetUsers()
        {
            return service.GetAllUsers().ToList();
        }

        [HttpGet("{userId}")]
        public ActionResult<UserDTO> GetUserById(int userId)
        {
            var user = service.GetUserById(userId);

            if(user == null)
            {
                return NotFound();
            }

            return user;
        }

        [HttpPost]
        public ActionResult CreateUser(UserDTO user)
        {
            service.CreateUser(user);
            return Ok();
        }

        [HttpPut]
        public ActionResult UpdateUser(UserDTO user)
        {
            var existingUser = service.GetUserById(user.Id);

            if (existingUser == null)
            {
                return NotFound();
            }

            service.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete("{userId}")]
        public ActionResult DeleteUser(int userId)
        {
            var userToDelete = service.GetUserById(userId);

            if (userToDelete == null)
            {
                return NotFound();
            }

            service.DeleteUser(userId);
            return NoContent();
        }

        [HttpGet("usersByABCWithSortedTasks")]
        public ActionResult<IEnumerable<UserDTO>> UsersByABCWithTasksSortedByName()
        {
            var users = service.UsersByABCWithTasksSortedByName().ToList();
            return users;
        }

        [HttpGet("GetUsersCharacteristics/{userId}")]
        public ActionResult<UserWithParameters> GetUsersCharacteristics(int userId)
        {
            var userWithParameters = service.GetUsersCharacteristics(userId);
            return userWithParameters;
        }
    }
}